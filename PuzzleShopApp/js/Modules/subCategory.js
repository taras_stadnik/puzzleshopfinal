﻿angular
.module('subCategories.module', ['factory.module', ])

.controller('subCategoryCtrl', subCategoryCtrl)

function subCategoryCtrl($scope, itemTableFactory, $rootScope, $stateParams) {

    $scope.itemName = [];
   
    $scope.getCategoryId = function () {
        $scope.itemName =  $stateParams.category;
        
        itemTableFactory.getsubcategory($scope.itemName).success(function (data) {
            $rootScope.CategoryItems = data;
            console.log($rootScope.CategoryItems);
            //console.log($scope.CategoryItems);
        }).error(function (data) {
            $scope.error = "An Error has occured while Loading items! " + data.ExceptionMessage;
        });
    }

    $scope.getCategoryId();

    $scope.mode = 1;
};