﻿angular
.module('categories.module', ['factory.module',])

.controller('categoryCtrl', categoryCtrl)

function categoryCtrl($scope, itemTableFactory, $rootScope, $stateParams) {
    
    $scope.getCategory = function () {
        itemTableFactory.getcategorylist().success(function (data) {
            $scope.items2 = data;
            console.log($scope.items2);
        }).error(function (data) {
            $scope.error = "An Error has occured while Loading items! " + data.ExceptionMessage;
        });
    }

     //get Subcategory Items
    $scope.getSubItems = function (item2) {
        $scope.itemId = item2;
        itemTableFactory.getsubItems($scope.itemId).success(function (data) {
            $rootScope.CategoryItems2 = data;
            console.log(data);
        }).error(function (data) {
            $scope.error = "An Error has occured while Loading items! " + data.ExceptionMessage;
        });
    }
    $scope.getCategory();
   
    $scope.mode = 1;
};
