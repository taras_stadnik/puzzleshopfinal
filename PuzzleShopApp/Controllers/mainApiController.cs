﻿
using PuzzleShopApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Description;

namespace PuzzleShopApp.Controllers
{
    

    public class mainApiController : ApiController
    {
         private PuzzleShopEntities db = new PuzzleShopEntities();

        //Получаем новинки
      
         public IQueryable<ItemsTable> GetItemsTable()
         {
             List<int> x = new List<int>(); ;
             db.Configuration.ProxyCreationEnabled = false;
             List<ItemsTable> items = new List<ItemsTable>();
             List<ItemsTable> itemsdb = new List<ItemsTable>();
            foreach(var item in db.ItemsTables)
            {
                itemsdb.Add(item);
            }
             
             for (int i = db.ItemsTables.Count() - 1; i > db.ItemsTables.Count() - 9; i--)
             {
                 items.Add(itemsdb[i]);         
             }     
             return items.AsQueryable();
         }
        //Получаем список категории
        public IQueryable<Category> GetCategoryTable()
        {
            db.Configuration.ProxyCreationEnabled = false;

            return db.Categories.AsQueryable();
        }

        //Получаем список подкатегорий
        //public IHttpActionResult GetSubCategoryTable([FromUri]string data)
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    List<Subcategory> subcategory = new List<Subcategory>();
        //    //foreach (var item in db.Subcategories)
        //    //{
        //    //    if (item.CategoryId == catNa)
        //    //    {
        //    //        subcategory.Add(item);
        //    //    }
        //    //}
        //    return Ok(subcategory);
        //}
        [Route("api/mainApi/GetSubCategoryTable/{data}")]
        public IQueryable GetSubCategoryTable([FromUri]string data)
        {
            db.Configuration.ProxyCreationEnabled = false;
            /*
            List<Category> category = new List<Category>();
            List<Subcategory> subcategory2 = new List<Subcategory>();
            Subcategory subcategory = new Subcategory(); 
            int id = 0;
            foreach (var item in db.Categories)
            {
                if (item.product_Category == data)
                {
                    id = item.Id;
                    break;
                }
            }
            foreach (var subitem in db.Subcategories)
            {
                if (subitem.CategoryId == id)
                {
                    //subcategory.Add(subitem);
                    //subcategory.Id = subitem.Id;
                    //subcategory.CategoryId = subitem.CategoryId;
                    //subcategory.SubName = subitem.SubName;
                    subcategory2.Add(subitem);
                }
              
               
            }
             */

            //db.Categories.Where(cat => cat.product_Category == data)
            
            /*
            var res = (from c in db.Categories
                       where c.Subcategories.Any (s => s.CategoryId == c.Id)
                       select c)
                       .SelectMany(c => c.Subcategories)
                       .Where((s => s.product_Category == data);
            return Ok(res);

             * */
            Category res = db.Categories.Where( c => c.product_Category == data).First<Category>();
            var subs = db.Subcategories.Where(s => s.CategoryId == res.Id).Select(a => new
            {
                name = a.SubName,
                id = a.Id
            });
           
           

            return subs;
        }

        [ResponseType(typeof(ItemsTable))]
        [HttpPost]
        public IHttpActionResult PostSubItems(Subcategory subName)
        {
            db.Configuration.ProxyCreationEnabled = false;
            int subId = 0;
     
            List<ItemsTable> subItems = new List<ItemsTable>();     
                subId = subName.Id;
            foreach (var item2 in db.ItemsTables)
            {
                if (item2.SubcategoryId == subId)
                {
                    subItems.Add(item2);
                }
            }
            return Ok(subItems);
        }
    }
}
